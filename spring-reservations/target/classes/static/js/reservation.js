/**
 * Script to handle placing reservations.
 */

"use strict";

(function() {
  
  function collectFormInput() {
    
    var reservationFormValues = {};
    $.each($('#reservationForm').serializeArray(), function(i, field) {
      reservationFormValues[field.name] = field.value;
    });
    
    return reservationFormValues;
  }
  
  
	document.getElementById("date").innerHTML = todayDate();
	function todayDate(){
		var today = new Date();
	    var dd = today.getDate();
	    var mm = today.getMonth()+1; //January is 0!
	    var yyyy = today.getFullYear();

	    if(dd<10) {
	        dd = '0'+dd
	    } 

	    if(mm<10) {
	        mm = '0'+mm
	    } 
	    
	    today = yyyy + '-' + mm + '-' + dd;
	    return today;
	}
$(document).ready(function() {

  $("#submitReservation").click(function(e) {     
    e.preventDefault();           

    $('.message').hide();
    
    var reservationRequest = collectFormInput();
    var errorMessage="The following errors occurred:\n\n";
    if(reservationRequest.resourceId<=0){
    	errorMessage+=" - The Facility ID must be positive integer!\n\n";
    }
    var today=todayDate();
    if(reservationRequest.fromDate<today){
    	if(reservationRequest.fromDate==""){
        	errorMessage+=" - Please Enter Start Date!\n\n";
    	}else{
        	errorMessage+=" - You can not Place Reservation in the Past!\n\n";
    	}
    }
    if(reservationRequest.toDate<reservationRequest.fromDate){
    	 if(reservationRequest.toDate==""){
    	    	errorMessage+=" - Please Enter End Date!\n\n";
    	    }else{
    	    	errorMessage+=" - Start Date must be Before End Date!\n\n"
    	    }
    }
   
    if(reservationRequest.owner==""){
    	errorMessage+=" - Please Enter your name!";
    }
    if(errorMessage!="The following errors occurred:\n\n"){
    	alert(errorMessage);
    	return;
    }
    
    var reservationUrl = 
      '/api/reservations/' + encodeURIComponent(reservationRequest.resourceId) 
      + '/from-' + encodeURIComponent(reservationRequest.fromDate) 
      + '/to-' + encodeURIComponent(reservationRequest.toDate)
      + '/?owner=' + encodeURIComponent(reservationRequest.owner);

    $.ajax({
      url : reservationUrl,                    
      method: 'POST',                           
      async : true,                          
      cache : false,                         
      timeout : 5000,                     

      data : {},                               

      success : function(data, statusText, response) {          
        var reservationId = response.getResponseHeader('reservation-id');
        alert("\nYour reservation has been successfully processed. \n\nYour reservation id is: "+reservationId);
        $('#reservationId').text(reservationId);
        $('#reservation-successful-message').show();
        location.reload();
      },
  
      error : function(XMLHttpRequest, textStatus, errorThrown) {   
        console.log("reservation request failed ... HTTP status code: " + XMLHttpRequest.status + ' message ' + XMLHttpRequest.responseText);
        
        var errorCodeToHtmlIdMap = {400 : '#validation-error', 405 : '#validation-error', 409 : '#conflict-error' , 500: '#system-error'};
        var id = errorCodeToHtmlIdMap[XMLHttpRequest.status];
      
        if (!id) {
          id =  errorCodeToHtmlIdMap[500]; 
        }
        var text="";
        switch(id){
        case '#validation-error': text="Reservation failed - please check your input for completeness."; break;
        case '#system-error': text="Reservation failed - please try again later."; break;
        case '#conflict-error': text="The room is already reserved for this period, please choose a different period."; break;
        }
        alert(text);
       
      }
    });
    
  });

  
});


})();